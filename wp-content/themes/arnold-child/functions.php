<?php

function ux_frontend_styles() {
	wp_register_style('customstyle-css', get_stylesheet_directory_uri() . '/custom.css', array(), '1.0', 'screen');
	wp_enqueue_style('customstyle-css');
}

add_action('wp_enqueue_scripts', 'ux_frontend_styles',102);  

/*
* Add your own functions here. You can also copy some of the theme functions into this file. 
* Wordpress will use those functions instead of the original functions then.
*/

?>