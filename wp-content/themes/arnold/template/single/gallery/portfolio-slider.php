<?php
//** get portfolio image
$portfolio = arnold_get_post_meta(get_the_ID(), 'theme_meta_portfolio');

if($portfolio){ ?>

    <div class="blog-unit-gallery-wrap single-fullwidth-slider-wrap">
    
        <div class="owl-carousel" data-item="3" data-center="true" data-margin="30" data-autowidth="true" data-slideby="1" data-showdot="false" data-nav="true" data-loop="true" data-lazy="false">
            
            <?php foreach($portfolio as $image){
                $thumb = wp_get_attachment_image($image, 'arnold-standard-thumb-medium');
			?>
                
                <section class="item">
                    <div class="carousel-img-wrap"><?php echo balanceTags($thumb); ?></div>
                </section>
            <?php } ?>
         </div> 
    </div>

<?php } ?>