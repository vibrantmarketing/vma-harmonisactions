<?php

/*
============================================================================
	*
	* require functions theme 
	*
============================================================================	
*/
require_once get_template_directory() . '/functions/theme/theme-admin.php';

/*
============================================================================
	*
	* require class 
	*
============================================================================	
*/
require_once get_template_directory() . '/functions/class/class-admin.php';

/*
============================================================================
	*
	* require interface 
	*
============================================================================	
*/
require_once get_template_directory() . '/functions/interface/interface-admin.php';

?>